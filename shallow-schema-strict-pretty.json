{
  "properties": {
    "description": {
      "maxLength": 1400,
      "type": "string",
      "fieldDescription": "\n\nA human readable description of what the add-on does. The description will be visible in the `Manage Add-ons`\n section of the administration console. Provide meaningful and identifying information for the instance administrator.\n\n"
    },
    "version": {
      "type": "string",
      "fieldDescription": "\n\n\u003cb\u003eNOTE\u003c/b\u003e This field is reserved for Atlassian Marketplace. Any value provided will be ignored.\n\n The version of the add-on. Upon registration of a new add-on version in Atlassian Marketplace, a value for this\n field is generated and subsequently provided to the Universal Plugin Manager.\n\n"
    },
    "modules": {
      "additionalProperties": true,
      "type": "object",
      "fieldDescription": "\n\nThe list of modules this add-on provides.\n\n"
    },
    "lifecycle": {
      "properties": {
        "installed": {
          "format": "uri",
          "type": "string",
          "fieldDescription": "\n\nWhen a Connect add-on is installed, a synchronous request is fired to this URL to initiate the installation\n handshake. In order to successfully complete installation, the add-on must respond with either a\n \u003ccode\u003e200 OK\u003c/code\u003e or \u003ccode\u003e204 No Content\u003c/code\u003e status.\n\n\u003cdiv class\u003d\"aui-message warning\"\u003e\n    \u003cp class\u003d\"title\"\u003e\n        \u003cspan class\u003d\"aui-icon icon-warning\"\u003eWarning\u003c/span\u003e\n        \u003cstrong\u003eImportant\u003c/strong\u003e\n    \u003c/p\u003e\n    Upon successful registration, the add-on must return either a \u003ccode\u003e200 OK\u003c/code\u003e or \u003ccode\u003e204 No Content\u003c/code\u003e\n    response code, otherwise the operation will fail and the installation will be marked as incomplete.\n\u003c/div\u003e\n\n"
        },
        "disabled": {
          "format": "uri",
          "type": "string",
          "fieldDescription": "\n\nFires when an add on has been successfully disabled. This is an asynchronous notification event.\n\n"
        },
        "uninstalled": {
          "format": "uri",
          "type": "string",
          "fieldDescription": "\n\nFires when an add on has been successfully un-installed. This is an asynchronous notification event.\n\n"
        },
        "enabled": {
          "format": "uri",
          "type": "string",
          "fieldDescription": "\n\nFires when an add on has been successfully enabled. This is an asynchronous notification event.\n\n"
        }
      },
      "additionalProperties": false,
      "shortClassName": "lifecycleBean",
      "type": "object",
      "title": "Lifecycle",
      "description": "\n\nAllows an add-on to register callbacks for events that occur in the lifecycle of an installation. When a lifecycle\n event is fired, a POST request will be made to the appropriate URL registered for the event.\n\n The \u003ca href\u003d\"#installed\"\u003e\u003ccode\u003einstalled\u003c/code\u003e\u003c/a\u003e lifecycle callback is an integral part of the installation process\n of an add-on, whereas the remaining lifecycle events are essentially \u003ca href\u003d\"../modules/webhook/\"\u003ewebhooks\u003c/a\u003e.\n\n Each property in this object is a URL relative to the add-on\u0027s base URL.\n\n \u003ch2\u003eLifecycle attribute example\u003c/h2\u003e\n\n\n\n\n\n    {\n      \"installed\": \"/installed\",\n      \"uninstalled\": \"/uninstalled\",\n      \"enabled\": \"/enabled\",\n      \"disabled\": \"/disabled\"\n    }\n\n\n\n\n \u003ch2\u003eLifecycle HTTP request payload\u003c/h2\u003e\n Lifecycle callbacks contain a JSON data payload with important tenant information that you will need to store in your\n add-on in order to sign and verify future requests. The payload contains the following attributes:\n\n\n\n\n\n    {\n      \"key\": \"installed-addon-key\",\n      \"clientKey\": \"unique-client-identifier\",\n      \"sharedSecret\": \"a-secret-key-not-to-be-lost\",\n      \"serverVersion\": \"server-version\",\n      \"pluginsVersion\": \"version-of-connect\",\n      \"baseUrl\": \"http://example.atlassian.net\",\n      \"displayUrl\": \"http://jira.example.net\",\n      \"productType\": \"jira\",\n      \"description\": \"Atlassian Jira at https://example.atlassian.net\",\n      \"serviceEntitlementNumber\": \"SEN-number\",\n      \"eventType\": \"installed\",\n      \"displayUrlServicedeskHelpCenter\": \"http://jira.example.net/servicedesk\"\n    }\n\n\n\n\n\u003ctable summary\u003d\"Lifecycle payload attributes\" class\u003d\u0027aui\u0027\u003e\n    \u003cthead\u003e\n        \u003ctr\u003e\n            \u003cth\u003eAttribute\u003c/th\u003e\n            \u003cth\u003eDescription\u003c/th\u003e\n        \u003c/tr\u003e\n    \u003c/thead\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003ekey\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eAdd-on key that was installed into the Atlassian Product, as it appears in your add-on\u0027s descriptor.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003eclientKey\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eIdentifying key for the Atlassian product instance that the add-on was installed into. This will never change for a given\n        instance, and is unique across all Atlassian product tenants. This value should be used to key tenant details\n        in your add-on. The one time the clientKey can change is when a backup taken from a different instance is restored onto the instance.\n        Determining the contract between the instance and add-on in this situation is tracked by\n        \u003ca href\u003d\"https://ecosystem.atlassian.net/browse/AC-1528\"\u003eAC-1528\u003c/a\u003e in the Connect backlog.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003esharedSecret\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eUse this string to sign outgoing JWT tokens and validate incoming JWT tokens. Optional: and may not\n        be present on non-JWT add-on installations, and is only sent on the \u003ccode\u003einstalled\u003c/code\u003e event.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003eserverVersion\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eThis is a string representation of the host product\u0027s version. Generally you should not need it.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003epluginsVersion\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eThis is a semver compliant version of Atlassian Connect which is running on the host server, for example: \u003ccode\u003e1.1.15\u003c/code\u003e.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003ebaseUrl\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eURL prefix for this Atlassian product instance. All of its REST endpoints begin with this `baseUrl`.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003eproductType\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eIdentifies the category of Atlassian product, e.g. \u003ccode\u003ejira\u003c/code\u003e or \u003ccode\u003econfluence\u003c/code\u003e.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003edescription\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eThe host product description - this is customisable by an instance administrator.\u003c/td\u003e\n    \u003c/tr\u003e\n    \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003eserviceEntitlementNumber\u003c/code\u003e\n        (optional)\u003c/td\u003e\n        \u003ctd\u003eAlso known as the SEN, the service entitlement number is the add-on license id. This attribute will only be included\n        during installation of a paid add-on.\u003c/td\u003e\n    \u003c/tr\u003e\n      \u003ctr\u003e\n        \u003ctd\u003e\u003ccode\u003eoauthClientId\u003c/code\u003e\u003c/td\u003e\n        \u003ctd\u003eThe OAuth 2.0 client ID for your add-on. For more information, see \u003ca href\u003d\"../oauth-2-jwt-bearer-tokens-for-add-ons/\"\u003eOAuth 2.0 - JWT Bearer token authorization grant type\u003c/a\u003e \u003c/td\u003e\n    \u003c/tr\u003e\n\u003c/table\u003e\n\n \u003ch2\u003eRequest query parameters\u003c/h2\u003e\n\n \u003cp\u003eWhen this payload information is POSTed to your Atlassian Connect service you may receive the \u003ccode\u003euser\\_key\u003c/code\u003e and \u003ccode\u003euser\\_id\u003c/code\u003e\n query parameters; these query parameters are both deprecated. \u003cspan class\u003d\"aui-lozenge aui-lozenge-current\"\u003eDeprecated\u003c/span\u003e\n If you wish to find out which user enabled your add-on then you should extract the \u003ccode\u003econtext.user.userKey\u003c/code\u003e or \u003ccode\u003econtext.user.userName\u003c/code\u003e\n data from the JWT token on the enabled lifecycle event.\u003c/p\u003e\n\n",
      "fieldDescription": "\n\nAllows the add on to register for plugin lifecycle notifications\n\n"
    },
    "lastUpdated": {
      "properties": {},
      "additionalProperties": false,
      "shortClassName": "date",
      "type": "object",
      "fieldDescription": "\n\n"
    },
    "baseUrl": {
      "pattern": "^((https|http):\\/\\/.+|)$",
      "maxLength": 200,
      "format": "uri",
      "type": "string",
      "fieldDescription": "\n\nThe base url of the remote add-on, which is used for all communications back to the add-on instance. Once the add-on is installed in a product, the add-on\u0027s baseUrl\n cannot be changed without first uninstalling the add-on or upgrading to a new version. This is important; choose your baseUrl wisely before making your add-on public.\n\n The baseUrl must start with ``https://`` to ensure that all data is sent securely between our [cloud instances](../../understanding-jwt/)\n and your add-on.\n\n Note: each add-on must have a unique baseUrl. If you would like to serve multiple add-ons from the same host, consider adding a path prefix into the baseUrl.\n\n"
    },
    "apiMigrations": {
      "properties": {
        "gdpr": {
          "type": "boolean",
          "fieldDescription": "\n\nEnables the use of GDPR-compliant JWTs and iframe URLs.\n\n",
          "defaultValue": "false"
        }
      },
      "additionalProperties": false,
      "shortClassName": "apiMigrationsBean",
      "type": "object",
      "title": "Api Migrations",
      "description": "\n\nAllows an add-on to opt-into an active api migration.\n\n Goal of this feature is to support API changes that could not be made backward compatible.\n Such changes will break existing apps that do not yet support new API version.\n The opt-in mechanism allows app vendors to communicate to host product what API changes they support.\n\n \u003cp\u003eSee \u003ca href\u003d\"../connect-active-api-migrations/\"\u003eactive migrations page\u003c/a\u003e for the list of currently active API migrations.\u003c/p\u003e\n\n\n\n\n\n    {\"apiMigrations\":{\"gdpr\":true}}\n\n\n\n\n For more info about this feature see \u003ca href\u003d\"../connect-api-migration/\"\u003eAPI migration Opt-in documentation\u003c/a\u003e.\n\n",
      "fieldDescription": "\n\nThe flags for the apiMigrations that this addon has opted into.\n\n"
    },
    "apiVersion": {
      "type": "integer",
      "fieldDescription": "\n\nThe API version is an OPTIONAL integer. If omitted we will infer an API version of 1.\n\n The intention behind the API version is to allow vendors the ability to beta test a major revision to their Connect add-on as a private version,\n and have a seamless transition for those beta customers (and existing customers) once the major revision is launched.\n\n Vendors can accomplish this by listing a new private version of their add-on, with a new descriptor hosted at a new URL.\n\n They use the Atlassian Marketplace\u0027s access token facilities to share this version with customers (or for internal use).\n When this version is ready to be taken live, it can be transitioned from private to public, and all customers will be seamlessly updated.\n\n It\u0027s important to note that this approach allows vendors to create new versions manually, despite the fact that in the common case, the versions are automatically created.\n This has a few benefits-- for example, it gives vendors the ability to change their descriptor URL if they need to\n (the descriptor URL will be immutable for existing versions)\n\n"
    },
    "vendor": {
      "$ref": "#/definitions/vendor",
      "fieldDescription": "\n\nThe vendor who is offering the add-on\n\n"
    },
    "translations": {
      "properties": {
        "paths": {
          "properties": {
            "ru-RU": {
              "format": "uri",
              "type": "string",
              "title": "ru-RU",
              "fieldDescription": "\n\nTranslations for the \"ru-RU\" (Russian) locale.\n\n"
            },
            "de-DE": {
              "format": "uri",
              "type": "string",
              "title": "de-DE",
              "fieldDescription": "\n\nTranslations for the \"de-DE\" (German) locale.\n\n"
            },
            "pt-PT": {
              "format": "uri",
              "type": "string",
              "title": "pt-PT",
              "fieldDescription": "\n\nTranslations for the \"pt-PT\" (Portuguese) locale.\n\n"
            },
            "en-US": {
              "format": "uri",
              "type": "string",
              "title": "en-US",
              "fieldDescription": "\n\nTranslations for the \"en-US\" (American English) locale.\n\n"
            },
            "ko-KR": {
              "format": "uri",
              "type": "string",
              "title": "ko-KR",
              "fieldDescription": "\n\nTranslations for the \"ko-KR\" (Korean) locale.\n\n"
            },
            "es-ES": {
              "format": "uri",
              "type": "string",
              "title": "es-ES",
              "fieldDescription": "\n\nTranslations for the \"es-ES\" (Spanish) locale.\n\n"
            },
            "en-UK": {
              "format": "uri",
              "type": "string",
              "title": "en-UK",
              "fieldDescription": "\n\nTranslations for the \"en-UK\" (British English) locale.\n\n"
            },
            "fr-FR": {
              "format": "uri",
              "type": "string",
              "title": "fr-FR",
              "fieldDescription": "\n\nTranslations for the \"fr-FR\" (French) locale.\n\n"
            },
            "ja-JP": {
              "format": "uri",
              "type": "string",
              "title": "ja-JP",
              "fieldDescription": "\n\nTranslations for the \"ja-JP\" (Japanese) locale.\n\n"
            }
          },
          "additionalProperties": false,
          "shortClassName": "translationPathsBean",
          "type": "object",
          "title": "Translation Paths",
          "description": "\n\nThis object is used to specify the locations of translations files.\n It is used in the \u003ca href\u003d\"../internationalization/\"\u003etranslations\u003c/a\u003e property of the add-on descriptor.\n\n Every property in this object is optional. The location defined in each property can be relative to the base URL of the add-on or absolute.\n\n \u003cp\u003e\u003cb\u003eExample\u003c/b\u003e\u003c/p\u003e\n\n\n\n\n\n    {\n      \"en-US\": \"/i18n/1.0/en_US.json\",\n      \"en-UK\": \"/i18n/1.0/en_UK.json\",\n      \"fr-FR\": \"/i18n/1.0/fr_FR.json\",\n      \"de-DE\": \"/i18n/1.0/de_DE.json\",\n      \"pt-PT\": \"/i18n/1.0/pt_PT.json\",\n      \"es-ES\": \"/i18n/1.0/es_ES.json\",\n      \"ja-JP\": \"/i18n/1.0/ja_JP.json\",\n      \"ko-KR\": \"/i18n/1.0/ko_KR.json\",\n      \"ru-RU\": \"/i18n/1.0/ru_RU.json\"\n    }\n\n\n",
          "fieldDescription": "\n\nThe URLs of the translation files for your add-on.\n\n"
        }
      },
      "required": [
        "paths"
      ],
      "additionalProperties": false,
      "shortClassName": "translationsBean",
      "type": "object",
      "title": "Translations",
      "description": "\n\nDefines the locations of any translation files for your add-on.\n A translation file maps the localization keys for UI modules in your add-on descriptor to translated strings.\n You will need one translation file per language. To learn more, read \u003ca href\u003d\"../modules/i18n/\"\u003e Internationalization\u003c/a\u003e.\n\n \u003cp\u003e\u003cb\u003eExample\u003c/b\u003e\u003c/p\u003e\n\n\n\n\n\n    {\n      \"paths\": {\n        \"en-US\": \"/i18n/1.0/en_US.json\",\n        \"en-UK\": \"/i18n/1.0/en_UK.json\",\n        \"fr-FR\": \"/i18n/1.0/fr_FR.json\",\n        \"de-DE\": \"/i18n/1.0/de_DE.json\",\n        \"pt-PT\": \"/i18n/1.0/pt_PT.json\",\n        \"es-ES\": \"/i18n/1.0/es_ES.json\",\n        \"ja-JP\": \"/i18n/1.0/ja_JP.json\",\n        \"ko-KR\": \"/i18n/1.0/ko_KR.json\",\n        \"ru-RU\": \"/i18n/1.0/ru_RU.json\"\n      }\n    }\n\n\n",
      "fieldDescription": "\n\n[Translations](../modules/i18n/) for this add-on.\n\n\n\n\n\n    {\n      \"translations\": {\n        \"paths\": {\n          \"en-US\": \"/i18n/1.0/en_US.json\",\n          \"en-UK\": \"/i18n/1.0/en_UK.json\",\n          \"fr-FR\": \"/i18n/1.0/fr_FR.json\",\n          \"de-DE\": \"/i18n/1.0/de_DE.json\",\n          \"pt-PT\": \"/i18n/1.0/pt_PT.json\",\n          \"es-ES\": \"/i18n/1.0/es_ES.json\",\n          \"ja-JP\": \"/i18n/1.0/ja_JP.json\",\n          \"ko-KR\": \"/i18n/1.0/ko_KR.json\",\n          \"ru-RU\": \"/i18n/1.0/ru_RU.json\"\n        }\n      }\n    }\n\n\n"
    },
    "installedDate": {
      "properties": {},
      "additionalProperties": false,
      "shortClassName": "date",
      "type": "object",
      "fieldDescription": "\n\n"
    },
    "enableLicensing": {
      "type": "boolean",
      "fieldDescription": "\n\nWhether or not to enable licensing options in the UPM/Marketplace for this add on\n\n",
      "defaultValue": "false"
    },
    "name": {
      "maxLength": 80,
      "type": "string",
      "fieldDescription": "\n\nThe human-readable name of the add-on\n\n"
    },
    "links": {
      "additionalProperties": true,
      "type": "object",
      "fieldDescription": "\n\nA set of links that the add-on wishes to publish\n\n\n\n\n\n    {\n      \"links\": {\n        \"self\": \"https://addon.domain.com/atlassian-connect.json\",\n        \"documentation\": \"https://addon.domain.com/docs\"\n      }\n    }\n\n\n"
    },
    "scopes": {
      "items": {
        "enum": [
          "none",
          "NONE",
          "read",
          "READ",
          "write",
          "WRITE",
          "delete",
          "DELETE",
          "project_admin",
          "PROJECT_ADMIN",
          "space_admin",
          "SPACE_ADMIN",
          "admin",
          "ADMIN",
          "act_as_user",
          "ACT_AS_USER",
          "access_email_addresses",
          "ACCESS_EMAIL_ADDRESSES"
        ],
        "type": "string"
      },
      "type": "array",
      "fieldDescription": "\n\nSet of [scopes](../../scopes/) requested by this add on\n\n\n\n\n\n    {\n      \"scopes\": [\n        \"read\",\n        \"write\"\n      ]\n    }\n\n\n"
    },
    "key": {
      "pattern": "^[a-zA-Z0-9-._]+$",
      "maxLength": 80,
      "type": "string",
      "fieldDescription": "\n\nA unique key to identify the add-on.\n This key must be \u0026lt;\u003d 80 characters, must only contain alphanumeric characters, dashes, underscores, and dots.\n\n"
    },
    "authentication": {
      "properties": {
        "type": {
          "enum": [
            "jwt",
            "JWT",
            "none",
            "NONE"
          ],
          "type": "string",
          "fieldDescription": "\n\nThe type of authentication to use.\n\n",
          "defaultValue": "jwt"
        }
      },
      "additionalProperties": false,
      "shortClassName": "authenticationBean",
      "type": "object",
      "title": "Authentication",
      "description": "\n\nDefines the authentication type to use when signing requests from the host application to the Connect add-on.\n\n \u003cp\u003e\u003cb\u003eExample\u003c/b\u003e\u003c/p\u003e\n\n\n\n\n\n    {\n      \"authentication\": {\n        \"type\": \"jwt\"\n      }\n    }\n\n\n",
      "fieldDescription": "\n\nDefines the authentication type to use when signing requests between the host application and the connect add on.\n\n"
    }
  },
  "required": [
    "baseUrl",
    "key",
    "authentication"
  ],
  "additionalProperties": false,
  "shortClassName": "shallowConnectAddonBean",
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "title": "Add-On Descriptor",
  "description": "\n\n\u003cp\u003eThe add-on descriptor is a JSON file (\u003ccode\u003eatlassian-connect.json\u003c/code\u003e) that describes the add-on to the Atlassian application.\n The descriptor includes general information for the add-on, as well as the modules that the add-on wants to use or\n extend.\u003c/p\u003e\n\n \u003cp\u003eIf you\u0027re familiar with Java add-on development with previous versions of the Atlassian Plugin Framework, you may already be\n familiar with the `atlassian-plugin.xml` descriptors. The `atlassian-connect.json` serves the same function.\u003c/p\u003e\n\n \u003cp\u003eThe descriptor serves as the glue between the remote add-on and the Atlassian application. When an administrator for a\n cloud instance installs an add-on, what they are really doing is installing this descriptor file, which\n contains pointers to your service. You can see an example below.\u003c/p\u003e\n\n \u003cp\u003eFor details and application-specific reference information on the descriptor please refer to the \"jira modules\"\n and \"confluence modules\" sections of this documentation. But we\u0027ll call out a few highlights from the example here.\u003c/p\u003e\n\n \u003cp\u003eSince Atlassian Connect add-ons are remote and largely independent from the Atlassian application, they can be changed\n at any time, without having to create a new version or report the change to the Atlassian instance. The changes are\n reflected in the Atlassian instance immediately (or at least at page reload time).\u003c/p\u003e\n\n \u003cp\u003eHowever, some add-on changes require a change to the descriptor file itself. For example, an add-on could be modified\n to have a new page module. Since this requires a page module declaration in the descriptor, it means making an updated\n descriptor available. Until that updated descriptor is re-installed into the Atlassian Product that change in the descriptor\n file will not take effect. To propagate a change in the descriptor to the Atlassian products, you need to create a new version\n of the add-on in its Marketplace listing. The Marketplace will take care of the rest: informing administrators\n and automatically installing the available update. See [Upgrades](/platform/marketplace/upgrading-and-versioning-cloud-apps/) for more details.\u003c/p\u003e\n\n \u003cdiv class\u003d\"aui-message aui-message-info\"\u003e\n     \u003cp class\u003d\"title\"\u003e\n         \u003cstrong\u003eValidating your descriptor\u003c/strong\u003e\n     \u003c/p\u003e\n     \u003cp\u003eYou can validate your descriptor using this \u003ca href\u003d\"https://atlassian-connect-validator.herokuapp.com/validate\"\u003ehandy tool\u003c/a\u003e.\u003c/p\u003e\n \u003c/div\u003e\n\n \u003cp\u003e\u003cb\u003eExample\u003c/b\u003e\u003c/p\u003e\n\n\n\n\n\n    {\n      \"modules\": {},\n      \"key\": \"my-addon-key\",\n      \"dependencies\": {},\n      \"name\": \"My Connect Addon\",\n      \"description\": \"A connect addon that does something\",\n      \"vendor\": {\n        \"name\": \"My Company\",\n        \"url\": \"http://www.example.com\"\n      },\n      \"links\": {\n        \"self\": \"http://www.example.com/connect/jira\"\n      },\n      \"lifecycle\": {\n        \"installed\": \"/installed\",\n        \"uninstalled\": \"/uninstalled\"\n      },\n      \"baseUrl\": \"http://www.example.com/connect/jira\",\n      \"authentication\": {\n        \"type\": \"jwt\"\n      },\n      \"enableLicensing\": true,\n      \"scopes\": [\n        \"read\",\n        \"write\"\n      ],\n      \"translations\": {\n        \"paths\": {\n          \"en-US\": \"/i18n/1.0/en_US.json\",\n          \"en-UK\": \"/i18n/1.0/en_UK.json\",\n          \"fr-FR\": \"/i18n/1.0/fr_FR.json\",\n          \"de-DE\": \"/i18n/1.0/de_DE.json\",\n          \"pt-PT\": \"/i18n/1.0/pt_PT.json\",\n          \"es-ES\": \"/i18n/1.0/es_ES.json\",\n          \"ja-JP\": \"/i18n/1.0/ja_JP.json\",\n          \"ko-KR\": \"/i18n/1.0/ko_KR.json\",\n          \"ru-RU\": \"/i18n/1.0/ru_RU.json\"\n        }\n      },\n      \"apiMigrations\": {}\n    }\n\n\n",
  "definitions": {
    "vendor": {
      "properties": {
        "name": {
          "maxLength": 100,
          "type": "string",
          "fieldDescription": "\n\nThe name of the plugin vendor.\n Supply your name or the name of the company you work for.\n\n"
        },
        "url": {
          "pattern": "^((https|http):\\/\\/.+|[^:]+|)$",
          "maxLength": 200,
          "format": "uri",
          "type": "string",
          "fieldDescription": "\n\nThe url for the vendor\u0027s website\n\n"
        }
      },
      "shortClassName": "vendorBean",
      "type": "object",
      "title": "Plugin Vendor",
      "description": "\n\nGives basic information about the plugin vendor.\n\n \u003ch3\u003eExample\u003c/h3\u003e\n\n\n\n\n\n    {\n      \"vendor\": {\n        \"name\": \"Atlassian\",\n        \"url\": \"http://www.atlassian.com\"\n      }\n    }\n\n\n"
    }
  }
}